# HttpMockServer #

HttpMock will allow to mock a HTTP server. It's main purpose is to be used for testing consumers of a HTTP-Api.

The Project is just started and will grow soon including documentation about it.

## How to build / contribute
* Clone
* Run `dotnet build`
* Create a topic branch
* Implement new feature / improvment / bugfix and tests
* Send a pull request