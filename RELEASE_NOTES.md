#### 2.0.0
Support .net Standard 1.2

#### 1.1.0
Add specification for HTTP method DELETE

#### 1.0.1
Improving documentation

#### 1.0.0
Assert that a request definition is only added once

#### 0.2.0
Log request and provide the log for assertions

#### 0.1.1
Remove dependency on JetBrains.Annotations

#### 0.1.0
Get the project up and running