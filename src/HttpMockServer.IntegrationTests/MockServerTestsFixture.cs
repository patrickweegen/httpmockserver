﻿using System;
using System.Net.Http;

namespace HttpMockServer.IntegrationTests
{
    public class MockServerTestsFixture : IDisposable
    {
        public MockServerTestsFixture()
        {
            Server = MockServer.Create();
            HttpClient = new HttpClient
            {
                BaseAddress = new Uri($"http://localhost:{Server.Port}/")
            };
        }

        public MockServer Server { get; }
        public HttpClient HttpClient { get; }

        public void Dispose()
        {
            HttpClient.Dispose();
            Server.Dispose();
        }
    }
}