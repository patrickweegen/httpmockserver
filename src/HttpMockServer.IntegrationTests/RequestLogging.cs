﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using HttpMockServer.IntegrationTests.Helpers;
using HttpMockServer.RequestResponse;
using Xunit;

namespace HttpMockServer.IntegrationTests
{
    public class RequestLogging : IDisposable
    {
        public RequestLogging()
        {
            _server = MockServer.Create();
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.OK)
                                                         .WithHeader("content-type", "text/plain")
                                                         .WithBody("Body")));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPut()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.OK)
                                                         .WithHeader("content-type", "text/plain")
                                                         .WithBody("Body")));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPost()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.OK)
                                                         .WithHeader("content-type", "text/plain")
                                                         .WithBody("Body")));

            _server.Register(Request.OnPath("/Foo")
                                    .UsingDelete()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.OK)));
            _server.Start();

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri($"http://localhost:{_server.Port}/")
            };
        }

        public void Dispose()
        {
            _httpClient.Dispose();
            _server.Dispose();
        }

        private readonly MockServer _server;
        private readonly HttpClient _httpClient;

        [Fact]
        public async Task Should_drop_all_locked_requests_When_Reset_is_called()
        {
            await _httpClient.GetAsync("/Foo");
            await _httpClient.PutAsync("/Foo", new StringContent("Body", Encoding.UTF8, "text/plan"));
            await _server.RequestLog.ResetAsync();
            await _httpClient.PostAsync("/Foo", new StringContent("Body", Encoding.UTF8, "text/plan"));

            var log = await _server.RequestLog.GetLogAsync();

            log.Should()
               .HaveCount(1);
            log.First()
               .Method.Should()
               .Be("POST");
        }

        [Fact]
        public async Task Should_log_the_made_response_completly()
        {
            await _httpClient.PostAsync("/Foo", new StringContent("Body", Encoding.UTF8, "text/plan"));

            var log = await _server.RequestLog.GetLogAsync();

            log.Should()
               .HaveCount(1);
            var loggedRequest = log.First();
            loggedRequest.Method.Should()
                         .Be("POST");
            loggedRequest.Path.Should()
                         .Be("/Foo");
            loggedRequest.Body.Should()
                         .Be("Body");
            loggedRequest.Headers.Should()
                         .Contain("Content-Type", "text/plan; charset=utf-8");
        }

        [Fact]
        public async Task Should_log_the_requests_in_the_right_order()
        {
            await _httpClient.GetAsync("/Foo");
            await _httpClient.PutAsync("/Foo", new StringContent("Body", Encoding.UTF8, "text/plan"));
            await _httpClient.PostAsync("/Foo", new StringContent("Body", Encoding.UTF8, "text/plan"));
            await _httpClient.DeleteAsync("/Foo");

            var log = await _server.RequestLog.GetLogAsync();

            log.Should()
               .HaveCount(4);
            log.First()
               .Method.Should()
               .Be("GET");
            log.Second()
               .Method.Should()
               .Be("PUT");
            log.Third()
               .Method.Should()
               .Be("POST");
            log.Fourth()
               .Method.Should()
               .Be("DELETE");
        }
    }
}