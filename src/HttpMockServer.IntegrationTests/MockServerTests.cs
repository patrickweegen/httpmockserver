﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using FluentAssertions;
using HttpMockServer.RequestResponse;
using Xunit;

namespace HttpMockServer.IntegrationTests
{
    public class MockServerTests : IDisposable
    {
        private readonly MockServer _server;
        private readonly HttpClient _httpClient;

        public MockServerTests()
        {
            _server = MockServer.Create();
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri($"http://localhost:{_server.Port}/")
            };
        }

        public void Dispose()
        {
            _httpClient.Dispose();
            _server.Dispose();
        }

        [Theory]
        [InlineData("/Bar")]
        [InlineData("/Foo/Bar")]
        public async Task Should_respond_with_NotFound_When_no_matching_request_definition_is_found(string requestUri)
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPut()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPost()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingDelete()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));

            _server.Start();

            var response = await _httpClient.GetAsync(requestUri);

            response.StatusCode.Should()
                    .Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public void Should_not_throw_any_exceptions_When_the_same_Request_gets_added_with_different_httpMethods_1()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingPost()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .NotThrow();
        }

        [Fact]
        public void Should_not_throw_any_exceptions_When_the_same_Request_gets_added_with_different_httpMethods_2()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPut()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingPost()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .NotThrow();
        }

        [Fact]
        public void Should_not_throw_any_exceptions_When_the_same_Request_gets_added_with_different_httpMethods_3()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPost()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingGet()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .NotThrow();
        }

        [Fact]
        public void Should_not_throw_any_exceptions_When_the_same_Request_gets_added_with_different_httpMethods_4()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingDelete()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingGet()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .NotThrow();
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_first_confiuration_for_path_contains_header_response_does()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Start();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Foo");
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_first_confiuration_for_path_contains_header_response_does_not()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_first_confiuration_for_path_contains_no_header_response_does()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));

            _server.Start();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Foo");
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_first_confiuration_for_path_contains_no_header_response_does_not()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));

            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_requests_with_multiple_headers_are_configured_1()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            _server.Start();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Foo");
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_requests_with_multiple_headers_are_configured_2()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));

            _server.Start();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Foo");
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_correct_response_When_requests_with_multiple_headers_are_configured_3()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Unauthorized)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            _server.Start();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Foo");
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.NotImplemented);
        }

        [Fact]
        public async Task Should_respond_with_header_and_body_When_both_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)
                                                         .WithHeader("content-type", "text/plain")
                                                         .WithBody("Body")));
            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            var responseContent = response.Content;
            responseContent.Headers.Should()
                           .Contain(pair => pair.Key.ToLower() == "content-type" &&
                                            pair.Value.Single()
                                                .ToLower() ==
                                            "text/plain");
            var bodyAsString = await responseContent.ReadAsStringAsync();
            bodyAsString.Should()
                        .Be("Body");
        }

        [Fact]
        public async Task Should_respond_with_header_When_header_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)
                                                         .WithHeader("content-type", "application/json")));
            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            response.Content.Headers.Should()
                    .Contain(pair => pair.Key.ToLower() == "content-type" &&
                                     pair.Value.Single()
                                         .ToLower() ==
                                     "application/json");
        }

        [Fact]
        public async Task Should_respond_with_NotFound_When_configured_header_is_not_in_the_request()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Foo")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Should_respond_with_NotFound_When_nothing_is_registered()
        {
            _server.Start();
            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Should_respond_with_the_configured_statuscode_When_delete_request_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingDelete()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Start();

            var response = await _httpClient.DeleteAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_the_configured_statuscode_When_get_request_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Start();

            var response = await _httpClient.GetAsync("/Foo");

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_the_configured_statuscode_When_post_request_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPost()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Start();

            var response = await _httpClient.PostAsync("/Foo", new StringContent("Bar"));

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public async Task Should_respond_with_the_configured_statuscode_When_put_request_is_configured()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPut()
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.Accepted)));
            _server.Start();

            var response = await _httpClient.PutAsync("/Foo", new StringContent("Bar"));

            response.StatusCode.Should()
                    .Be(HttpStatusCode.Accepted);
        }

        [Fact]
        public void Should_throw_InvalidOperationException_When_the_same_Request_gets_added_twice_1()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingGet()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingGet()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .Throw<InvalidOperationException>()
                        .Which.Message.Should()
                        .Be("This request was already added.");
        }

        [Fact]
        public void Should_throw_InvalidOperationException_When_the_same_Request_gets_added_twice_2()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPut()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingPut()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .Throw<InvalidOperationException>()
                        .Which.Message.Should()
                        .Be("This request was already added.");
        }

        [Fact]
        public void Should_throw_InvalidOperationException_When_the_same_Request_gets_added_twice_3()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingPost()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingPost()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .Throw<InvalidOperationException>()
                        .Which.Message.Should()
                        .Be("This request was already added.");
        }

        [Fact]
        public void Should_throw_InvalidOperationException_When_the_same_Request_gets_added_twice_4()
        {
            _server.Register(Request.OnPath("/Foo")
                                    .UsingDelete()
                                    .WithHeader("Authorization", "Bearer Foo")
                                    .WithHeader("Accept", "application/json")
                                    .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented)));

            var actionToFail = new Action(() => _server.Register(Request.OnPath("/Foo")
                                                                        .UsingDelete()
                                                                        .WithHeader("Authorization", "Bearer Foo")
                                                                        .WithHeader("Accept", "application/json")
                                                                        .RespondWith(Response.WithStatusCode(HttpStatusCode.NotImplemented))));
            actionToFail.Should()
                        .Throw<InvalidOperationException>()
                        .Which.Message.Should()
                        .Be("This request was already added.");
        }
    }
}