﻿using System.Collections.Generic;
using System.Linq;

namespace HttpMockServer.IntegrationTests.Helpers
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Returns the second element in the enumeration.
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/> of the element within the enumeration.</typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <returns>The second element in the enumeration.</returns>
        public static T Second<T>(this IEnumerable<T> enumeration)
        {
            return enumeration.ElementAt(1);
        }

        /// <summary>
        /// Returns the third element in the enumeration.
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/> of the element within the enumeration.</typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <returns>The third element in the enumeration.</returns>
        public static T Third<T>(this IEnumerable<T> enumeration)
        {
            return enumeration.ElementAt(2);
        }

        /// <summary>
        /// Returns the fourth element in the enumeration.
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/> of the element within the enumeration.</typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <returns>The fourth element in the enumeration.</returns>
        public static T Fourth<T>(this IEnumerable<T> enumeration)
        {
            return enumeration.ElementAt(3);
        }
    }
}