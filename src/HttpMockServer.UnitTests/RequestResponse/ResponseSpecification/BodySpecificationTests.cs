﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse.ResponseSpecification;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse.ResponseSpecification
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class BodySpecificationTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidBody)
        {
            var actionToFail = new Action(() => new BodySpecification(invalidBody));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Apply_Should_throw_ArgumentNullException_When_called_without_response()
        {
            var specification = new BodySpecification("body");

            var actionToFail = new Action(() => specification.Apply(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullException_When_created_without_body()
        {
            var actionToFail = new Action(() => new BodySpecification(null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}