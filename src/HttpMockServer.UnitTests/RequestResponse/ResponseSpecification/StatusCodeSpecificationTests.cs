﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using FluentAssertions;
using HttpMockServer.RequestResponse.ResponseSpecification;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse.ResponseSpecification
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class StatusCodeSpecificationTests
    {
        [Fact]
        public void Apply_Should_throw_ArgumentNullException_When_no_response_is_given()
        {
            var specification = new StatusCodeSpecification(HttpStatusCode.OK);

            var actionToFail = new Action(() => specification.Apply(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}