﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse.ResponseSpecification;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse.ResponseSpecification
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class HeaderSpecificationTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidName)
        {
            var actionToFail = new Action(() => new HeaderSpecification(invalidName, "value"));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Apply_Should_throw_ArgumentNullException_When_no_response_is_given()
        {
            var specification = new HeaderSpecification("name", "value");

            var actionToFail = new Action(() => specification.Apply(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_name()
        {
            var actionToFail = new Action(() => new HeaderSpecification(null, "value"));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_value()
        {
            var actionToFail = new Action(() => new HeaderSpecification("name", null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}