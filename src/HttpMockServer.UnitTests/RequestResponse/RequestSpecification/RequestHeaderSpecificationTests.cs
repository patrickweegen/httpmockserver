﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse.RequestSpecification;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse.RequestSpecification
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "MissingAnnotation", Justification = "It's a test class.")]
    public class RequestHeaderSpecificationTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidName)
        {
            var actionToFail = new Action(() => new RequestHeaderSpecification(invalidName, "value"));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Match_Should_throw_ArgumentNullException_When_called_without_request()
        {
            var specification = new RequestHeaderSpecification("name", "value");

            var actionToFail = new Action(() => specification.Match(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_name()
        {
            var actionToFail = new Action(() => new RequestHeaderSpecification(null, "value"));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_value()
        {
            var actionToFail = new Action(() => new RequestHeaderSpecification("name", null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}