﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse.RequestSpecification;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse.RequestSpecification
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class PutSpecificationTests
    {
        [Fact]
        public void Match_Should_throw_ArgumentNullExcpetion_When_no_request_is_given()
        {
            var specification = new PutSpecification();

            var actionToFail = new Action(() => specification.Match(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}