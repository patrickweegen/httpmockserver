﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using FluentAssertions;
using HttpMockServer.RequestResponse;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "MissingAnnotation", Justification = "It's a test class.")]
    public class ResponseDefinitionTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void WithBody_Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidBody)
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);
            var actionToFail = new Action(() => definition.WithBody(invalidBody));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidName)
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);

            var actionToFail = new Action(() => definition.WithHeader(invalidName, "value"));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Apply_Should_throw_ArgumentNullException_When_called_without_response()
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);

            var actionToFail = new Action(() => definition.Apply(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_name()
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);

            var actionToFail = new Action(() => definition.WithHeader(null, "value"));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_value()
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);

            var actionToFail = new Action(() => definition.WithHeader("name", null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void WithBody_Should_throw_ArgumentNullException_When_created_without_body()
        {
            var definition = new ResponseDefinition(HttpStatusCode.OK);
            var actionToFail = new Action(() => definition.WithBody(null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}