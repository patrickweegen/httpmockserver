﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "MissingAnnotation", Justification = "It's a test class.")]
    public class RequestDefinitionTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeExcpetion_When_created_with_empty_path(string invalidPath)
        {
            var actionToFail = new Action(() => new RequestDefinition(invalidPath));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void WithHeader_Should_throw_ArgumentOutOfRangeException_When_created_with_invalid_name(string invalidName)
        {
            var definition = new RequestDefinition("/Foo");
            var actionToFail = new Action(() => definition.WithHeader(invalidName, "value"));
            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Match_Should_throw_ArgumentNullExcpetion_When_called_without_request()
        {
            var definition = new RequestDefinition("/Foo");

            var actionToFail = new Action(() => definition.Match(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullExcpetion_When_created_without_path()
        {
            var actionToFail = new Action(() => new RequestDefinition(null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void WithHeader_Should_throw_ArgumentNullExcpetion_When_created_without_name()
        {
            var definition = new RequestDefinition("/Foo");
            var actionToFail = new Action(() => definition.WithHeader(null, "value"));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void WithHeader_Should_throw_ArgumentNullExcpetion_When_created_without_value()
        {
            var definition = new RequestDefinition("/Foo");
            var actionToFail = new Action(() => definition.WithHeader("name", null));
            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}