﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using HttpMockServer.RequestResponse;
using Xunit;

namespace HttpMockServer.UnitTests.RequestResponse
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "MissingAnnotation", Justification = "It's a test class.")]
    public class RequestTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void OnPath_Should_throw_ArgumentException_When_path_is_empty_or_whitspace(string path)
        {
            var actionToFail = new Action(() => Request.OnPath(path));

            actionToFail.Should()
                        .Throw<ArgumentException>();
        }

        [Fact]
        public void OnPath_Should_throw_ArgumentNullException_When_path_is_null()
        {
            var actionToFail = new Action(() => Request.OnPath(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}