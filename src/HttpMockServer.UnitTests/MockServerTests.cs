﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace HttpMockServer.UnitTests
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class MockServerTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-80)]
        public void Create_with_port_Should_throw_ArgumentOutOfRangeException_When_invalid_port_is_given(int invalidPort)
        {
            var actionToFail = new Action(() => MockServer.Create(invalidPort));

            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Create_Should_return_new_instance_of_MockServer_with_initialized_Port_When_Port_is_given()
        {
            var mockServer = MockServer.Create(4711);

            mockServer.Should()
                      .BeOfType<MockServer>();
            mockServer.Port.Should()
                      .Be(4711);

            mockServer.Dispose();
        }

        [Fact]
        public void Create_without_port_Should_determine_free_port_and_return_a_new_instance_When_no_port_is_given()
        {
            var mockServer = MockServer.Create();

            mockServer.Port.Should()
                      .BeGreaterThan(0);

            mockServer.Dispose();
        }

        [Fact]
        public void Register_Should_throw_ArgumentNullException_When_called_without_RequestDefinition()
        {
            var mockServer = MockServer.Create();
            var actionToFail = new Action(() => mockServer.Register(null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}