﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace HttpMockServer.UnitTests
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute", Justification = "It's a test class.")]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement", Justification = "It's a test class.")]
    public class TinyHttpServerTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void Should_throw_ArgumentOutOfRangeException_When_uriPrefix_is_empty_or_whitespace(string invalidUriPrefix)
        {
            var actionToFail = new Action(() => new TinyHttpServer(invalidUriPrefix, context => { }));

            actionToFail.Should()
                        .Throw<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData("ftp://localhost")]
        [InlineData("smtp://localhost")]
        [InlineData("http://localhost:9999")]
        public void Should_throw_ArgumentException_When_uriPrefix_is_not_valid(string invalidUriPrefix)
        {
            var actionToFail = new Action(() => new TinyHttpServer(invalidUriPrefix, context => { }));

            actionToFail.Should()
                        .Throw<ArgumentException>();
        }

        [Fact]
        public async Task Should_call_given_http_handler_When_started_and_request_is_received()
        {
            var called = false;

            const string uriPrefix = "http://localhost:9005/";
            var server = new TinyHttpServer(uriPrefix, context =>
            {
                called = true;
                context.Response.Close();
            });

            server.Start();

            var httpClient = new HttpClient();
            await httpClient.GetAsync(uriPrefix);

            httpClient.Dispose();
            server.Dispose();

            called.Should()
                  .BeTrue();
        }

        [Fact]
        public void Should_throw_ArgumentNullException_When_created_without_httpHandler()
        {
            var actionToFail = new Action(() => new TinyHttpServer("http://localhost:9999", null));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }

        [Fact]
        public void Should_throw_ArgumentNullException_When_created_without_uriPrefix()
        {
            var actionToFail = new Action(() => new TinyHttpServer(null, context => { }));

            actionToFail.Should()
                        .Throw<ArgumentNullException>();
        }
    }
}