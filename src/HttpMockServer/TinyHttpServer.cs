﻿using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

[assembly:InternalsVisibleTo("HttpMockServer.UnitTests")]

namespace HttpMockServer
{
    internal sealed class TinyHttpServer : IDisposable
    {
        [NotNull] private readonly Action<HttpListenerContext> _httpHandler;
        [NotNull] private readonly HttpListener _listener;
        [NotNull] private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        /// <summary>
        ///     Creates a new instance of <see cref="TinyHttpServer" /> tied to the given <paramref name="uriPrefix" />.
        /// </summary>
        /// <param name="uriPrefix">Prefix for the URIs to listen to. The Uri has to be http or https.</param>
        /// <param name="httpHandler"><see cref="Action{T}" /> to handle the incoming http requests.</param>
        /// <example>
        ///     <code>var server = new TinyHttpServer("http://localhost:9900/", new Action&lt;HttpListenerContext&gt;(ctx => {}));</code>
        /// </example>
        /// <exception cref="ArgumentNullException"><paramref name="uriPrefix" /> is <see langword="null" /></exception>
        /// <exception cref="ArgumentNullException"><paramref name="httpHandler" /> is <see langword="null" /></exception>
        /// <exception cref="PlatformNotSupportedException">
        ///     This class cannot be used on the current operating system. Windows
        ///     Server 2003 or Windows XP SP2 is required to use instances of this class.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     <paramref name="uriPrefix" /> does not use the http:// or https:// scheme. These are the only schemes supported for
        ///     <see cref="T:System.Net.HttpListener" /> objects. -or-<paramref name="uriPrefix" /> is not a correctly formatted
        ///     URI prefix. Make sure the string is terminated with a "/".
        /// </exception>
        /// <exception cref="HttpListenerException">
        ///     A Windows function call failed. Check the exception's
        ///     <see cref="P:System.Net.HttpListenerException.ErrorCode" /> property to determine the cause of the exception. This
        ///     exception is thrown if another <see cref="T:System.Net.HttpListener" /> has already added the prefix
        ///     <paramref name="uriPrefix" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="uriPrefix" /> has to be provided.</exception>
        public TinyHttpServer(
            [NotNull] string uriPrefix,
            [NotNull] Action<HttpListenerContext> httpHandler)
        {
            if (uriPrefix == null) throw new ArgumentNullException(nameof(uriPrefix));
            if (string.IsNullOrWhiteSpace(uriPrefix)) throw new ArgumentOutOfRangeException(nameof(uriPrefix), uriPrefix, "uriPrefix has to be provided.");

            _httpHandler = httpHandler ?? throw new ArgumentNullException(nameof(httpHandler));
            _listener = new HttpListener();
            _listener.Prefixes.Add(uriPrefix);
        }

        /// <summary>
        ///     Starts listeneing for http requests.
        /// </summary>
        /// <exception cref="HttpListenerException">
        ///     A Win32 function call failed. Check the exception's
        ///     <see cref="P:System.Net.HttpListenerException.ErrorCode" /> property to determine the cause of the exception.
        /// </exception>
        /// <exception cref="ObjectDisposedException">This object is closed.</exception>
        public void Start()
        {
            _listener.Start();
            Task.Run(async () =>
                {
                    while (!_cts.Token.IsCancellationRequested)
                    {
                        var context = await _listener.GetContextAsync()
                                                     .ConfigureAwait(false);
                        _httpHandler(context);
                    }
                }
                , _cts.Token);
        }

        public void Dispose()
        {
            if (_listener.IsListening)
            {
                _cts.Cancel();
                _listener.Stop();
            }

            _listener.Abort();
            ((IDisposable) _listener).Dispose();
            _cts.Dispose();
        }
    }
}