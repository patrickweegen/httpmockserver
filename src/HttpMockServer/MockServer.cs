﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using HttpMockServer.RequestLog;
using HttpMockServer.RequestResponse;
using JetBrains.Annotations;

namespace HttpMockServer
{
    /// <inheritdoc />
    /// <summary>
    /// Mocked HTTP-Server
    /// </summary>
    [PublicAPI]
    public sealed class MockServer : IDisposable
    {
        [NotNull] private readonly TinyHttpServer _server;
        [NotNull] private readonly List<IRequestDefinition> _requestDefinitions = new List<IRequestDefinition>();
        [NotNull] private readonly RequestLogger _requestLog = new RequestLogger();

        /// <summary>
        ///     Creates a nes instance of <see cref="MockServer" />. <see cref="Port" /> to listen to after start will get
        ///     determined automatically.
        /// </summary>
        /// <returns>A new <see cref="MockServer" /> instance, not yet listening</returns>
        [NotNull]
        public static MockServer Create()
        {
            var port = FindFreeTcpPort();
            return Create(port);
        }

        /// <summary>
        ///     Creates a new instance of <see cref="MockServer" />. When started, listening to the given <paramref name="port" />.
        /// </summary>
        /// <param name="port">Number of the port, the created instance of <see cref="MockServer" /> should listen to.</param>
        /// <returns>A new <see cref="MockServer" /> instance, not yet listening to the given port.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="port" /> is not a valid port number.</exception>
        [NotNull]
        public static MockServer Create(int port)
        {
            if (port <= 0) throw new ArgumentOutOfRangeException(nameof(port), port, "A valid port has to provided.");

            return new MockServer(port);
        }

        // see http://stackoverflow.com/questions/138043/find-the-next-tcp-port-in-net
        private static int FindFreeTcpPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();

            var endpoint = (IPEndPoint) listener.LocalEndpoint;
            var port = endpoint.Port;

            listener.Stop();

            return port;
        }

        /// <exception cref="ArgumentOutOfRangeException"><paramref name="port" /> has to be greater 0.</exception>
        private MockServer(int port)
        {
            if (port <= 0) throw new ArgumentOutOfRangeException(nameof(port));
            Port = port;
            _server = new TinyHttpServer($"http://localhost:{port}/", HttpHandler);
        }

        /// <summary>
        ///     Port to whis get listened when started.
        /// </summary>
        public int Port { get; }

        /// <summary>
        ///     Request log for checking if requests were made correctly.
        /// </summary>
        [NotNull]
        public IRequestLog RequestLog => _requestLog;

        /// <exception cref="ArgumentNullException"><paramref name="context" /> is <see langword="null" /></exception>
        [SuppressMessage("ReSharper", "AvoidAsyncVoid", Justification = "It's an eventhandler.")]
        private async void HttpHandler([NotNull] HttpListenerContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

#pragma warning disable 4014 // Logging should not block execution
            await _requestLog.AddAsync(context.Request);
#pragma warning restore 4014

            var requestDefinition = _requestDefinitions.OrderByDescending(rd => rd.CountOfDefinedHeaders)
                                                       .FirstOrDefault(rd => rd.Match(context.Request));
            if (requestDefinition != null)
                requestDefinition.ResponseDefinition.Apply(context.Response);
            else
                context.Response.StatusCode = 404;

            context.Response.Close();
        }

        /// <summary>
        ///     Starts listening to the given <see cref="Port" />.
        /// </summary>
        public void Start()
        {
            _server.Start();
        }

        /// <summary>
        ///     Registers a definition of requests that should be answered by the mocked http server.
        /// </summary>
        /// <param name="requestDefinition"><see cref="IPathSpecified" /> that defines the request to be answered.</param>
        /// <exception cref="ArgumentNullException"><paramref name="requestDefinition" /> is <see langword="null" /></exception>
        public void Register([NotNull] IRequestDefinition requestDefinition)
        {
            if (requestDefinition == null) throw new ArgumentNullException(nameof(requestDefinition));

            if (_requestDefinitions.Any(x => x.Equals(requestDefinition)))
            {
                throw new InvalidOperationException("This request was already added.");
            }
            _requestDefinitions.Add(requestDefinition);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _server.Dispose();
        }
    }
}