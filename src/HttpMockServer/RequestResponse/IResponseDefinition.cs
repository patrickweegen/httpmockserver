using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    /// <summary>
    ///     Completly configured response to be added to an <see cref="IRequestDefinition" />
    /// </summary>
    [PublicAPI]
    public interface IResponseDefinition
    {
        /// <summary>
        ///     Adds a header to the response definition.
        /// </summary>
        /// <param name="name">Name of the header.</param>
        /// <param name="value">Value of the Header</param>
        [NotNull]
        IResponseDefinition WithHeader([NotNull] string name, [NotNull] string value);

        /// <summary>
        ///     Adds a body to the response definition.
        /// </summary>
        /// <param name="body">Content to be send as body.</param>
        [NotNull]
        IResponseDefinition WithBody([NotNull] string body);

        /// <summary>
        ///     Apply the definition to the given <paramref name="response" />
        /// </summary>
        /// <param name="response"><see cref="HttpListenerResponse" /> the settings will get applied to.</param>
        void Apply([NotNull] HttpListenerResponse response);
    }
}