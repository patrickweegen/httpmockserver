﻿using System;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    /// <summary>
    ///     Factory to build definitions of requests.
    /// </summary>
    [PublicAPI]
    public static class Request
    {
        /// <summary>
        ///     Defines a request that is made with the given <paramref name="path" />.
        /// </summary>
        /// <param name="path">Path to the requested resource.</param>
        /// <returns>A definition for the given path to be configured with further information about the request.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="path" /> is <see langword="null" /></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="path" /> is not provided.</exception>
        [NotNull]
        public static IPathSpecified OnPath([NotNull] string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentOutOfRangeException(nameof(path), path, "Path has to be provided.");

            return new RequestDefinition(path);
        }
    }
}