﻿using System;
using System.Linq;
using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse.RequestSpecification
{
    internal sealed class RequestHeaderSpecification : IRequestSpecification, IEquatable<RequestHeaderSpecification>
    {
        [NotNull] private readonly string _name;
        [NotNull] private readonly string _value;

        /// <exception cref="ArgumentNullException"><paramref name="name"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException">The <paramref name="name"/> of the header has to be given.</exception>
        /// <exception cref="ArgumentOutOfRangeException">The length of <paramref name="value"/> is greater than 65,535 characters.</exception>
        public RequestHeaderSpecification([NotNull] string name, [NotNull] string value)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentOutOfRangeException(nameof(name), name, "The name of the header has to be given.");
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value.Length > 65535) throw new ArgumentOutOfRangeException(nameof(value), value.Length, "The length of value is greater than 65,535 characters.");

            _name = name;
            _value = value;
        }

        /// <exception cref="ArgumentNullException"><paramref name="request"/> is <see langword="null"/></exception>
        public bool Match(HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var listenerHeaders = request.Headers;
            var headers = listenerHeaders.AllKeys.ToDictionary(k => k, k => listenerHeaders[k]);

            return headers.Keys.Contains(_name) && headers[_name] == _value;
        }

        public bool Equals(RequestHeaderSpecification other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(_name, other._name) && string.Equals(_value, other._value);
        }

        public bool Equals(IRequestSpecification other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() == GetType()) return true;
            return Equals(other as RequestHeaderSpecification);
        }

        public override bool Equals(object obj)
        {
            return Equals((IRequestSpecification)obj);
        }

        public override int GetHashCode()
        {
            return (_name.GetHashCode() * 397) ^ _value.GetHashCode();
        }
    }
}