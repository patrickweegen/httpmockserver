﻿using System;
using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse.RequestSpecification
{
    internal interface IRequestSpecification : IEquatable<IRequestSpecification>
    {
        bool Match([NotNull] HttpListenerRequest request);
    }
}