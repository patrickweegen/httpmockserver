﻿using System;
using System.Net;

namespace HttpMockServer.RequestResponse.RequestSpecification
{
    internal class GetSpecification : IRequestSpecification
    {
        public bool Match(HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            return request.HttpMethod.ToUpper() == "GET";
        }

        public bool Equals(IRequestSpecification other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.GetType() == GetType();
        }

        public override bool Equals(object obj)
        {
            return Equals((IRequestSpecification)obj);
        }

        public override int GetHashCode()
        {
            return GetType()
                .GetHashCode();
        }
    }
}