﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using HttpMockServer.RequestResponse.RequestSpecification;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    internal sealed class RequestDefinition :
        IPathSpecified,
        IMethodSpecified,
        IRequestDefinition, 
        IEquatable<RequestDefinition>
    {
        [NotNull] private readonly string _path;
        [NotNull] private readonly List<IRequestSpecification> _specifications = new List<IRequestSpecification>();

        /// <exception cref="ArgumentNullException"><paramref name="path" /> is <see langword="null" /></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="path" /> has to be provided.</exception>
        [SuppressMessage("ReSharper", "NotNullMemberIsNotInitialized", Justification="Property ResponseDefinition is initialized in factory method")]
        internal RequestDefinition([NotNull] string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentOutOfRangeException(nameof(path), path, "Path has to be provided.");

            _path = path;
        }

        public IResponseDefinition ResponseDefinition { get; private set; }
        public int CountOfDefinedHeaders => _specifications.OfType<RequestHeaderSpecification>()
                                                           .Count();

        internal void Add([NotNull] IRequestSpecification specification)
        {
            if (specification == null) throw new ArgumentNullException(nameof(specification));

            _specifications.Add(specification);
        }

        public IMethodSpecified UsingGet()
        {
            _specifications.Add(new GetSpecification());
            return this;
        }

        public IMethodSpecified UsingPut()
        {
            _specifications.Add(new PutSpecification());
            return this;
        }

        public IMethodSpecified UsingPost()
        {
            _specifications.Add(new PostSpecification());
            return this;
        }

        public IMethodSpecified UsingDelete()
        {
            _specifications.Add(new DeleteSpecification());
            return this;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="name" /> is <see langword="null" /></exception>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="value" /> is <see langword="null" /></exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="name" /> of the header has to be given.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     The length of <paramref name="value" /> is greater than 65,535
        ///     characters.
        /// </exception>
        public IMethodSpecified WithHeader(string name, string value)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentOutOfRangeException(nameof(name), name, "The name of the header has to be given.");
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value.Length > 65535)
                throw new ArgumentOutOfRangeException(nameof(value), value.Length, "The length of value is greater than 65,535 characters.");

            _specifications.Add(new RequestHeaderSpecification(name, value));
            return this;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="response" /> is null.</exception>
        public IRequestDefinition RespondWith(IResponseDefinition response)
        {
            ResponseDefinition = response ?? throw new ArgumentNullException(nameof(response));
            return this;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="request" /> is <see langword="null" /></exception>
        public bool Match(HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            if (!Equals(_path, request.RawUrl))
                return false;

            return _specifications.All(s => s.Match(request));
        }

        public bool Equals(RequestDefinition other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (!string.Equals(_path, other._path)) return false;
            if (_specifications.Count != other._specifications.Count) return false;
            return _specifications.TrueForAll(specification => other._specifications.Any(x => x.Equals(specification)));
        }

        public bool Equals(IRequestDefinition other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != GetType()) return false;
            return Equals(other as RequestDefinition);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as IRequestDefinition);
        }

        public override int GetHashCode()
        {
            return (_path.GetHashCode() * 397) ^ _specifications.GetHashCode();
        }
    }
}