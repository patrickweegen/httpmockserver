﻿using System;
using System.Collections.Generic;
using System.Net;
using HttpMockServer.RequestResponse.ResponseSpecification;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    internal class ResponseDefinition :
        IResponseDefinition
    {
        [NotNull] private readonly List<IResponseSpecification> _specifications = new List<IResponseSpecification>();

        public ResponseDefinition(HttpStatusCode statusCode)
        {
            _specifications.Add(new StatusCodeSpecification(statusCode));
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="name" /> is <see langword="null" /></exception>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="value" /> is <see langword="null" /></exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="name" /> is empty or whitespace.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="value" /> is greater than 65,535 characters.</exception>
        public IResponseDefinition WithHeader(string name, string value)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentOutOfRangeException(nameof(name), name, "The name of the header has to be given.");
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value.Length > 65535) throw new ArgumentOutOfRangeException(nameof(value), value.Length, "The length of value is greater than 65,535 characters.");

            _specifications.Add(new HeaderSpecification(name, value));

            return this;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="body" /> is <see langword="null" /></exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="body" /> is empty or whitespace.</exception>
        public IResponseDefinition WithBody(string body)
        {
            if (body == null) throw new ArgumentNullException(nameof(body));
            if (string.IsNullOrWhiteSpace(body)) throw new ArgumentOutOfRangeException(nameof(body), body, "A value for body has to be provided.");

            _specifications.Add(new BodySpecification(body));

            return this;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentNullException"><paramref name="response" /> is <see langword="null" /></exception>
        public void Apply(HttpListenerResponse response)
        {
            if (response == null) throw new ArgumentNullException(nameof(response));

            foreach (var specification in _specifications)
            {
                specification.Apply(response);
            }
        }
    }
}