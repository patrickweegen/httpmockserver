﻿using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    /// <summary>
    ///     Factory to build definitions of responses.
    /// </summary>
    [PublicAPI]
    public static class Response
    {
        /// <summary>
        ///     Defines a response with the given <paramref name="statusCode" />.
        /// </summary>
        /// <param name="statusCode"><see cref="HttpStatusCode" /> to be used with the response.</param>
        /// <returns>A definition of an response, to be cunfigured with further data.</returns>
        [NotNull]
        public static IResponseDefinition WithStatusCode(HttpStatusCode statusCode)
        {
            return new ResponseDefinition(statusCode);
        }
    }
}