﻿using System;
using System.Net;
using System.Text;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse.ResponseSpecification
{
    internal sealed class BodySpecification : IResponseSpecification
    {
        [NotNull] private readonly string _body;

        /// <exception cref="ArgumentNullException"><paramref name="body"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="body"/> is empty or whitespace.</exception>
        public BodySpecification([NotNull] string body)
        {
            if (body == null) throw new ArgumentNullException(nameof(body));
            if (string.IsNullOrWhiteSpace(body)) throw new ArgumentOutOfRangeException(nameof(body), body, "A value for body has to be provided.");

            _body = body;
        }

        /// <exception cref="ArgumentNullException"><paramref name="response"/> is <see langword="null"/></exception>
        public void Apply(HttpListenerResponse response)
        {
            if (response == null) throw new ArgumentNullException(nameof(response));

            var content = Encoding.UTF8.GetBytes(_body);
            response.OutputStream.Write(content, 0, content.Length);
        }
    }
}