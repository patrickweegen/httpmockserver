﻿using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse.ResponseSpecification
{
    internal interface IResponseSpecification
    {
        void Apply([NotNull] HttpListenerResponse response);
    }
}