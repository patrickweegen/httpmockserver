﻿using System;
using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse.ResponseSpecification
{
    internal sealed class HeaderSpecification : IResponseSpecification
    {
        [NotNull] private readonly string _name;
        [NotNull] private readonly string _value;

        /// <exception cref="ArgumentNullException"><paramref name="name"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException">The <paramref name="name"/> of the header has to be given.</exception>
        /// <exception cref="ArgumentOutOfRangeException">The length of <paramref name="value"/> is greater than 65,535 characters.</exception>
        public HeaderSpecification([NotNull] string name, [NotNull] string value)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentOutOfRangeException(nameof(name), name, "The name of the header has to be given.");
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (value.Length > 65535) throw new ArgumentOutOfRangeException(nameof(value), value.Length, "The length of value is greater than 65,535 characters.");

            _name = name;
            _value = value;
        }

        /// <exception cref="ArgumentNullException"><paramref name="response"/> is <see langword="null"/></exception>
        public void Apply(HttpListenerResponse response)
        {
            if (response == null) throw new ArgumentNullException(nameof(response));

            response.AddHeader(_name, _value);
        }
    }
}