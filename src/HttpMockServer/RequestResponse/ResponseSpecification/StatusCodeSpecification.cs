﻿using System;
using System.Net;

namespace HttpMockServer.RequestResponse.ResponseSpecification
{
    internal sealed class StatusCodeSpecification : IResponseSpecification
    {
        private readonly HttpStatusCode _statusCode;

        public StatusCodeSpecification(HttpStatusCode statusCode)
        {
            _statusCode = statusCode;
        }

        /// <exception cref="ArgumentNullException"><paramref name="response"/> is <see langword="null"/></exception>
        public void Apply(HttpListenerResponse response)
        {
            if (response == null) throw new ArgumentNullException(nameof(response));

            response.StatusCode = (int) _statusCode;
        }
    }
}