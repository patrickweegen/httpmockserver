﻿using System.Net;
using JetBrains.Annotations;

namespace HttpMockServer.RequestResponse
{
    /// <summary>
    ///     Completly configured request, including response definition.
    /// </summary>
    [PublicAPI]
    public interface IRequestDefinition
    {
        /// <summary>
        ///     Configured definition for response.
        /// </summary>
        [NotNull]
        IResponseDefinition ResponseDefinition { get; }

        /// <summary>
        ///     Number of the in the request contained headers.
        /// </summary>
        int CountOfDefinedHeaders { get; }

        /// <summary>
        ///     Does the <see cref="IRequestDefinition" /> match the given <paramref name="request" />?
        /// </summary>
        /// <param name="request">Request to be responded to.</param>
        /// <returns><c>true</c> if the request matches the </returns>
        bool Match([NotNull] HttpListenerRequest request);
    }

    /// <summary>
    ///     Path of request is specified, method is next.
    /// </summary>
    [PublicAPI]
    public interface IPathSpecified
    {
        /// <summary>
        ///     Defines that a GET-request should be answered
        /// </summary>
        [NotNull]
        IMethodSpecified UsingGet();

        /// <summary>
        ///     Defines that a PUT-request should be answered
        /// </summary>
        [NotNull]
        IMethodSpecified UsingPut();

        /// <summary>
        ///     Defines that a POST-request should be answered
        /// </summary>
        [NotNull]
        IMethodSpecified UsingPost();

        /// <summary>
        ///     Defines that a DELETE-request should be answered
        /// </summary>
        [NotNull]
        IMethodSpecified UsingDelete();
    }

    /// <summary>
    ///     HTTP-Method is specified, response is next.
    /// </summary>
    [PublicAPI]
    public interface IMethodSpecified
    {
        /// <summary>
        ///     Set response for configured request.
        /// </summary>
        /// <param name="response">Definition of response</param>
        [NotNull]
        IRequestDefinition RespondWith([NotNull] IResponseDefinition response);

        /// <summary>
        ///     Adds a header to the request definition.
        /// </summary>
        /// <param name="name">Name of the header.</param>
        /// <param name="value">Value of the Header</param>
        [NotNull]
        IMethodSpecified WithHeader([NotNull] string name, [NotNull] string value);
    }
}