﻿using System;
using JetBrains.Annotations;

namespace HttpMockServer.Utils
{
    internal class DisposableAction : IDisposable
    {
        /// <summary>
        /// Returns a new <see cref="DisposableAction"/> that invokes <paramref name="action"/> once <see cref="Dispose"/> is called.
        /// </summary>
        /// <param name="action">The <see cref="Action"/> to invoke</param>
        /// <returns>A new <see cref="DisposableAction"/></returns>
        [NotNull]
        public static DisposableAction InvokeOnDispose([NotNull] Action action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            return new DisposableAction(action);
        }

        [NotNull] private readonly Action _action;

        private DisposableAction([NotNull] Action action)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _action();
        }
    }
}