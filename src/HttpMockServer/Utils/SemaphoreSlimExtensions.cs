﻿using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace HttpMockServer.Utils
{
    /// <summary>
    /// Provides extension methods for <see cref="SemaphoreSlim"/>
    /// </summary>
    internal static class SemaphoreSlimExtensions
    {
        /// <summary>
        /// Asynchronously waits to enter the <see cref="SemaphoreSlim"/> and releases it when the returned <see cref="IDisposable"/> is disposed.
        /// </summary>
        /// <example>
        /// <code>
        /// using(await _semaphore.WaitAsyncAndRelease())
        /// {
        ///   //critical section
        /// }
        /// </code>
        /// </example>
        /// <param name="semaphore">The <see cref="SemaphoreSlim"/> to enter</param>
        /// <returns>A <see cref="Task"/> that returns an <see cref="IDisposable"/> which can be used to release the <paramref name="semaphore"/></returns>
        /// <exception cref="ArgumentNullException">Thrown if reference is <c>null</c>.</exception>
        /// <exception cref="ObjectDisposedException">The <see cref="SemaphoreSlim"/> instance has already been disposed.</exception>
        /// <exception cref="SemaphoreFullException">The <see cref="T:System.Threading.SemaphoreSlim" /> has already reached its maximum size.</exception>
        public static async Task<IDisposable> EnterAsync([NotNull] this SemaphoreSlim semaphore)
        {
            if (semaphore == null) throw new ArgumentNullException(nameof(semaphore));
            await semaphore.WaitAsync()
                           .ConfigureAwait(false);
            return DisposableAction.InvokeOnDispose(() => semaphore.Release());
        }
    }
}