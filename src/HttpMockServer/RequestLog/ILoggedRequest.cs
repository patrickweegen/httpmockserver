﻿using System.Collections.ObjectModel;
using JetBrains.Annotations;

namespace HttpMockServer.RequestLog
{
    /// <summary>
    ///     Logged request, could be used to check if requests are made correctly.
    /// </summary>
    [PublicAPI]
    public interface ILoggedRequest
    {
        /// <summary>
        ///     URL used for the request.
        /// </summary>
        [CanBeNull]
        string Path { get; }

        /// <summary>
        ///     HTTP method used for the request.
        /// </summary>
        [CanBeNull]
        string Method { get; }

        /// <summary>
        ///     All headers of the made request.
        /// </summary>
        [CanBeNull]
        ReadOnlyDictionary<string, string> Headers { get; }

        /// <summary>
        ///     Payload of the request.
        /// </summary>
        [CanBeNull]
        string Body { get; }
    }
}