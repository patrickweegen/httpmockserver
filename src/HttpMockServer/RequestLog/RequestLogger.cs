﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using HttpMockServer.Utils;
using JetBrains.Annotations;

namespace HttpMockServer.RequestLog
{
    internal class RequestLogger : IRequestLog
    {
        [NotNull] private readonly List<ILoggedRequest> _log = new List<ILoggedRequest>();
        [NotNull] private SemaphoreSlim _logSemaphore = new SemaphoreSlim(1);

        public async Task AddAsync([NotNull] HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var headers = GetHeaders(request);
            var body = GetBody(request);

            var loggedRequest = new LoggedRequest
            {
                Path = request.RawUrl,
                Method = request.HttpMethod,
                Headers = headers,
                Body = body,
            };


            using (await _logSemaphore.EnterAsync())
            {
                _log.Add(loggedRequest);
            }
        }

        [NotNull]
        private static ReadOnlyDictionary<string, string> GetHeaders([NotNull] HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var listenerHeaders = request.Headers;
            var headers = listenerHeaders.AllKeys.ToDictionary(k => k, k => listenerHeaders[k]);
            return new ReadOnlyDictionary<string, string>(headers);
        }

        [NotNull]
        private static string GetBody([NotNull] HttpListenerRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            if (!request.HasEntityBody)
            {
                return string.Empty;
            }

            using (var inputStream = request.InputStream)
            using (var reader = new StreamReader(inputStream, request.ContentEncoding))
            {
                return reader.ReadToEnd();
            }
        }

        public async Task<ReadOnlyCollection<ILoggedRequest>> GetLogAsync()
        {
            using (await _logSemaphore.EnterAsync())
            {
                return new ReadOnlyCollection<ILoggedRequest>(_log);
            }
        }

        public async Task ResetAsync()
        {
            using (await _logSemaphore.EnterAsync())
            {
                _log.Clear();
            }
        }
    }
}