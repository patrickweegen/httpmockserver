﻿using System.Collections.ObjectModel;

namespace HttpMockServer.RequestLog
{
    internal class LoggedRequest : ILoggedRequest
    {
        public string Path { get; set; }
        public string Method { get; set; }
        public ReadOnlyDictionary<string, string> Headers { get; set; }
        public string Body { get; set; }
    }
}