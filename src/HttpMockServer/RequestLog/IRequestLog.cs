﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace HttpMockServer.RequestLog
{
    /// <summary>
    ///     Thread safe log of all made requests.
    ///     The requests are logged in the order they were made.
    /// </summary>
    [PublicAPI]
    public interface IRequestLog
    {
        /// <summary>
        ///     Returns a <see cref="Task" /> to await the <see cref="ReadOnlyCollection{T}" /> with all
        ///     <see cref="ILoggedRequest" />.
        /// </summary>
        [NotNull]
        Task<ReadOnlyCollection<ILoggedRequest>> GetLogAsync();

        /// <summary>
        ///     Returns a <see cref="Task" /> to await the clearing of the log.
        ///     When the returned <see cref="Task" /> is completed the so far collected log is deleted.
        /// </summary>
        [NotNull]
        Task ResetAsync();
    }
}